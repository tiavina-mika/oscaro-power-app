import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Input, Icon } from 'react-native-elements';

import { isTablet, width } from '../../utils/utils';

const styles = StyleSheet.create({
    title: {
    },
    inputBlock1: {
      marginTop: isTablet? 40: 25,
    },
    inputBlock2: {
      marginTop: isTablet? 70: 50,
    },
    label: {
      fontSize: 16,
      lineHeight: 29
    },
    inputContainer: {
      backgroundColor: '#F0F0F0',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      borderRadius: 5,
      marginTop: 12,
    },
    inputContainerStyle: {
      borderBottomColor: 'transparent',
      backgroundColor: '#F0F0F0',
      height: 56,
      paddingHorizontal: 5
    },
    inputStyle: {
      height: 56,
      fontSize: 16
    },
    inputContainerStyle2: {
      width: isTablet? width - 150: width - 130,
    },
    inputContainerMeasure: {
      justifyContent: 'space-between',
      alignItems: 'center',
      width: width - 40
    },
    measureContainer: {
      marginRight: 20
    },
    measureText: {
      fontSize: 16,
      fontWeight: '600'
    },
    positionContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: 12
    },
    positonText: {
      color: '#7F7F7F',
      fontSize: 16,
      marginLeft: 6
    }
});

const YourSituationContent = () => {
    const [address, setAddress] = useState('');
    const [amount, setAmount] = useState('');
    return (
        <View>
            <View>
                <Text h4 style={styles.title}>Votre Situation</Text>
            </View>
            <View style={styles.inputBlock1}>
                <Text style={styles.label}>Où souhaitez-vous installer vos panneaux solaires</Text>
                <View style={styles.inputContainer}>
                    <Input
                        placeholder='Entrer votre adresse'
                        inputContainerStyle={styles.inputContainerStyle}
                        inputStyle={styles.inputStyle}
                        placeholderTextColor="#969696"
                        rightIcon={<Icon name='search' size={30} color='black' />}
                        onChangeText={text => setAddress(text)}
                        value={address}
                    />
                </View>
                <View style={styles.positionContainer}>
                    <Icon name="location-searching"color="#7F7F7F" size={24} />
                    <Text style={styles.positonText}>Utiliser ma position actuelle</Text>
                </View>
            </View>

            <View style={styles.inputBlock2}>
                <Text style={styles.label}>Quelle est le montant de votre facture électrique mensuelle</Text>
                <View style={[styles.inputContainer, styles.inputContainerMeasure]}>
                    <View >
                        <Input
                            placeholder='Ex: 129€'
                            inputContainerStyle={[styles.inputContainerStyle, styles.inputContainerStyle2]}
                            inputStyle={styles.inputStyle}
                            placeholderTextColor="#969696"
                            keyboardType="numeric"
                            onChangeText={text => setAmount(text)}
                            value={amount}
                        />
                    </View>
                    <View style={styles.measureContainer}>
                        <Text style={styles.measureText}>/ mois</Text>
                        </View>
                    </View>                  
                </View>
        </View>
    );
}

export default YourSituationContent;