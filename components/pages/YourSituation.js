import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Icon } from 'react-native-elements';

import Button from '../blocks/Button';
import RoofAnalysis from './RoofAnalysisDialog';
import YourSituationContent from './YourSituationContent';
import { width } from '../../utils/utils';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'flex-start',
      justifyContent: 'space-between',
    },
    containerPage: {
      backgroundColor: '#fff',
      paddingHorizontal: 20
    },
    containerDialog: {
      backgroundColor: '#999999',
      paddingHorizontal: 15
    },
    buttonContainer: {
      paddingBottom: 20
    },
    buttonContainerOnOpen: {
      paddingBottom: 20,
      position: 'absolute',
      bottom: 0,
      left: 17
    },
    iconBack: {
      marginLeft: 30,
    }
});

const YourSituation = ({ navigation }) => {
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(!open);
        navigation.setOptions({ 
            title: '',  
            headerStyle: { elevation: 0, shadowOpacity: 0, height: 55, backgroundColor: '#999' },
            headerLeft: () => <Icon name="arrow-back" />,
            headerLeftContainerStyle: { marginLeft: 12 }
        });
    };
    const handleClose = () => {
        setOpen(false);
        navigation.setOptions({ 
            title: '',  
            headerStyle: { elevation: 0, shadowOpacity: 0 } ,
            headerLeft: () => <Icon name="arrow-back" onPress={() => navigation.goBack()} />,
        });
    };

    return (
        <View style={[styles.container, open? styles.containerDialog: styles.containerPage]}>
              { open
                  ? <RoofAnalysis onClose={handleClose} />
                  : <YourSituationContent />
              }
              <View style={open? styles.buttonContainerOnOpen: styles.buttonContainer}>
                  <Button 
                      title="Continuer" 
                      onPress={!open? handleOpen: undefined} 
                      background={open}
                      width={open? width - 33: null}
                  /> 
              </View>
         </View>
    );
}

export default YourSituation;