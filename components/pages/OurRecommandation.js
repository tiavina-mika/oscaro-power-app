import React from 'react';
import { StyleSheet, View, ScrollView, TouchableHighlight } from 'react-native';
import { Text } from 'react-native-elements';

import KitRecommanded from '../our-recommandation/KitRecommanded';
import Prevision from '../our-recommandation/Prevision';
import BlockTitle from '../our-recommandation/BlockTitle';
import Slider from '../our-recommandation/Slider';

const styles = StyleSheet.create({
    root: {
        backgroundColor: '#fff',
    },
    container: {
        alignItems: 'center'
    },
    main: {
        width: '90%'
    },
    block1: {
        backgroundColor: '#F7F7F7',
        marginTop: 10,
        paddingVertical: 15,
        paddingHorizontal: 15,
    },
    title: {
        fontSize: 18,
        fontWeight: '700'
    },
    description: {
        color: '#494949'
    },
    block2: {
        marginTop: 30
    },
    mainTitle: {
        textTransform: 'uppercase',
        color: '#999999',
        fontSize: 16
    },
    footer: {
        alignItems: 'center',
        backgroundColor: '#fff',
        marginBottom: 20
    },
    button: {
        backgroundColor: '#000',
        width: '90%',
        paddingVertical: 15,
        paddingHorizontal: 20,
        alignItems: 'center',
        borderRadius: 3
    },
    buttonText: {
        color: '#fff'
    },
    slider: {
        marginTop: 12,
        marginBottom: 2
    }
});

const OurRecommandation = () => {

    return (
        <>
            <ScrollView style={styles.root}>
                <View style={styles.container}>
                    <View style={styles.main}>
                        <View style={styles.block1}>
                            <Text style={styles.title}>Nombre de panneaux solaire</Text>
                            <Text style={styles.description}>8 recommandé</Text>
                            <View style={styles.slider}>
                                <Slider />
                            </View>
                        </View>

                        <View style={styles.block2}>
                            <BlockTitle text="Kit recommandé" />
                        <KitRecommanded />
                        </View>

                        <View style={styles.block2}>
                            <BlockTitle text="Prévision" />
                            <Prevision />
                        </View>
                    </View>
                </View>
            </ScrollView>

             <View style={styles.footer}>
                <TouchableHighlight style={styles.button}>
                    <Text style={styles.buttonText}>Voir ma simulation et mes économies</Text>
                </TouchableHighlight>
             </View>

        </>
    );
}

export default OurRecommandation;