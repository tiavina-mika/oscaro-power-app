import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-elements';

import Button from '../blocks/Button';
import Link from '../blocks/Link';
import { isTablet, width } from '../../utils/utils';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'column',
    },
    logoContainer: {
        marginTop: 50,
    },
    rectContainer: {
        flex: isTablet? 1: 0,
        marginTop: isTablet? 130: 30
    },
    rect: {
        height: 194,
        width: isTablet? width - 100: 302,
        backgroundColor: '#EFEFEF',
    },
    descriptionContainer: {
        alignItems: 'center',
        justifyContent: 'flex-start',
        flex: isTablet? 1: 1.2,
        paddingHorizontal: isTablet? 60: 35,
    },
    description: {
        marginTop: isTablet? 0: 35,
        fontWeight: '700',
        textAlign: 'center',
        fontSize: 22
    },
    buttonContainer: {
        flex: 1,
    },
    linkContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 15
    },
    linkText: {
        fontSize: 16,
        color: '#999999',
        marginRight: 10
    }
});

const Home = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Text>Logo</Text>
            </View>
            <View style={styles.rectContainer}>
                <View style={styles.rect}>
                </View>
            </View>
            <View style={styles.descriptionContainer}>
                <Text style={styles.description}> Simuler vos économie grâce à votre futur kit solaire Oscaro Power</Text>
            </View>            
            <View style={styles.buttonContainer}>
                {/* <Button title="Commencer" onPress={() => navigation.navigate('OurRecommandation')}/> */}
                <Button title="Commencer" onPress={() => navigation.navigate('YourSituation')}/>
                <View style={styles.linkContainer}>
                    <Text style={styles.linkText}>Déjà client?</Text>
                    <Link text="Connectez-vous" bold/>
                </View>   
            </View>
        </View>
    );
}



export default Home;