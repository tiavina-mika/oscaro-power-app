import React, { useState, useEffect } from 'react';
import { StyleSheet, View, TouchableHighlight } from 'react-native';
import { Text, Icon, Image } from 'react-native-elements';
import Constants from 'expo-constants';
import { useInterval } from '../hooks/useInterval';

import { isTablet, width, sm, md, lg } from '../../utils/utils';

const contentWidth = width - 35;
const rayon = width - 100;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
        marginTop: Constants.statusBarHeight,
    },
    closeIconContainer: {
        alignItems: 'flex-end',
        marginRight: 15,
        marginTop: 22,
        marginBottom: 12,
    },
    descriptionContainer: {
        alignItems: 'center',
        overflow: 'hidden'
    },
    description: {
        width: contentWidth,
        alignItems: 'center',
        borderRadius: 12,
        backgroundColor: '#fff',
        overflow: 'hidden'
    },
    textContainer: {
        paddingHorizontal: 25,
        paddingTop: sm || md ? 13: 20,
        paddingBottom: sm || md ? 20: 30,
    },
    imageContainer: {
        // width: contentWidth,
        // height: 140,
    },
    image: {
        height: sm ? 104: lg? 181: 114.5,
        width: contentWidth
    },
    text: {
        color: '#000',
        textAlign: 'left',
        fontSize: sm || md  ? 16: 18
    },
    topContainer: {
        flex: 1
    },
    bottomContainer: {
        alignItems: 'center',
        justifyContent: isTablet? 'center': 'flex-start',
        flex: isTablet? 3: 1,
    },
    circleContainer: {
        height: rayon,
        width: rayon,
        borderRadius: rayon / 2,
        backgroundColor: '#fff',
        alignItems: 'flex-start',
        justifyContent: 'center',
        position: 'relative',
        overflow: 'hidden'
    },
    circle: {
        height: rayon,
        width: rayon,
        borderRadius: rayon / 2,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        zIndex: 100,
        top: 0,
        left: 0,
        opacity: 0.7,
    },
    loading: {
        zIndex: 102, 
        backgroundColor: '#999', 
        height: '100%',
        opacity: 0.2,     
    },
    textButtonContainer: {
        marginTop: 2
    },
    textButton: {
        fontSize: md? 26: lg? 32: 25,
        fontWeight: '600'
    }
});

const LaunchAnalyze = ({ route, navigation }) => {
    const [count, setCount] = useState(0);
    const [start, setStart] = useState(false);

    useEffect(() => {
        if (route.params) {
            setCount(route.params.start);
            setStart(false);
        }
    }, [route.params])

    useInterval(() => {
        if (start && count < 100) {
            setCount(count + 10);
        }
    }, 100);

    useEffect(() => {
        if (count === 100) {
            navigation.navigate('AnalyzeCompleted');
        }
    }, [count])

    const handleClick = () => setStart(!start);

    return (
        <View style={styles.container}>
            <View style={styles.topContainer}>
                <View style={styles.closeIconContainer}>
                    <Icon
                        name="close" 
                        color="#fff"
                        size={32}
                        onPress={() => navigation.goBack()}
                    />
                </View>
                <View style={styles.descriptionContainer}>
                    <View style={styles.description}>
                        <View style={styles.imageContainer}>
                            <Image
                                source={require('../../assets/images/cropped_roof.png')}
                                resizeMode="contain"
                                style={styles.image}
                            />                   
                        </View>
                        <View style={styles.textContainer}>
                            <Text style={styles.text}>
                                Placez-vous face à votre toit et imitez son inclinaison avec votre téléphone avant de lancer l'analyse
                            </Text>
                        </View>                   
                    </View>
                </View>                
            </View>
            <View style={styles.bottomContainer}>
                <TouchableHighlight onPress={handleClick}>
                    <View style={styles.circleContainer}>
                        <View style={styles.circle}>
                            <View>
                                <Icon 
                                    name="arrow-forward"
                                    size={isTablet? 45: 35}
                                />
                            </View>
                            <View style={styles.textButtonContainer}>
                                <Text style={styles.textButton}>Lancer l'analyse</Text>
                            </View>                        
                        </View>
                        <View
                            style={[styles.loading, { width: `${count}%` }]}
                        >
                        </View>
                    </View>                    
                </TouchableHighlight>

            </View>
        </View>
    );
}

export default LaunchAnalyze;