import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Icon } from 'react-native-elements';
import Constants from 'expo-constants';

import AnalyzeItem from '../blocks/AnalyzeItem';
import CardAction from '../blocks/CardAction';
import { isTablet, width, height, sm, md, lg } from '../../utils/utils';

const dialogWidth = isTablet? width - 50: width - 30;
const dialogHeight = md? height - 50: sm? height - 60: height - 80;
const bodyWidth = '85%';
const checkIconRayon = sm? 50: 68;

const styles = StyleSheet.create({
    root: {
        backgroundColor: '#000',
        marginTop: Constants.statusBarHeight,
        height: '100%'
    },
    container: {
        marginTop: sm? 15: 25,
        alignItems: 'center',
        justifyContent: 'center',
    },
    dialog: {
        backgroundColor: '#fff',
        width: dialogWidth,
        borderRadius: 10,
        justifyContent: 'space-between',
        height: dialogHeight,
        overflow: 'hidden'
    },
    content: {
        flex: 5,
        justifyContent: 'space-between'
    },
    closeIconContainer: {
        alignItems: 'flex-end',
        marginRight: sm? 5: 15,
        marginTop:  sm? 3: 15,
    },
    checkIconContainer: {
        alignItems: 'center',
    },
    checkIcon: {
        borderColor: '#F7F7F7',
        borderWidth: 1,
        height: checkIconRayon,
        width: checkIconRayon,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: checkIconRayon / 2
    },
    titleContainer: {
        alignItems: 'center',
        marginTop: sm? 0: 10,
        marginBottom: sm? 0: 15,
    },
    title: {
        fontWeight: '700',
        fontSize: 20
    },
    body: {
        justifyContent: 'space-between',
        flex: 10,
        marginTop: isTablet? 35: 15,
        alignItems: 'center',
    },
    itemsContainer: {
        justifyContent: !lg? 'space-between': 'flex-start',
        height: '100%',

        width: bodyWidth, 
    },
    action: {
        alignItems: 'center',
        flex: 1
    }
});

const items = [
    {
        icon:"home",
        title:"Inclinaison",
        value:"20 - 25°",
        description:"Votre inclinaison est optimale pour la production d'énergie solaire !",
    },
    {
        icon:"cursor-default",
        iconType: "material-community",
        title:"Orientation",
        value:"Sud",
        description:"Une orientation Sud/Sud-Est est optimale pour produire de l'énergie solaire toute l'année",
    },
    {
        icon:"white-balance-sunny",
        iconType: "material-community",
        title:"Ensoleillement",
        value:"Excellent",
        description:"L'ensoleillement de votre région est excellent pour accueillir une installation solaire",
    },  
]

const AnalyzeCompleted = ({ navigation }) => {
    return (
        <View style={styles.root}>
            <View style={styles.container}>
                <View style={styles.dialog}>
                    <View style={styles.content}>
                        <View style={styles.closeIconContainer}>
                            <Icon
                                name="close" 
                                color="#000"
                                size={sm? 22: 32}
                                onPress={() => navigation.navigate('LaunchAnalyze', { start: 0})}
                            />
                        </View>
                        <View style={styles.checkIconContainer}>
                            <View style={styles.checkIcon}>
                                <Icon
                                    name="check" 
                                    color="#000"
                                    size={sm? 27: 30}
                                />
                            </View>
                        </View>
                        <View style={styles.titleContainer}>
                            <Text style={styles.title}>Analyse terminée: Optimale</Text>
                        </View>
                        <View style={styles.body}>
                            <View style={styles.itemsContainer}>
                                { items.map((item, i) => (
                                    <AnalyzeItem
                                        lastItem={items.length - 1 === i}
                                        key={i}
                                        icon={item.icon}
                                        iconType={item.iconType}
                                        title={item.title}
                                        value={item.value}
                                        description={item.description}
                                    />                    
                                ))}                    
                            </View>                        
                        </View>

                    </View>
                    <View style={styles.action}>
                        <CardAction
                            width={bodyWidth}
                            linkText="Recommencer"
                            linkUrl="YourSituation"
                            buttonText="Valider"
                            icon="check"
                            buttonUrl="OurRecommandation"
                        />
                    </View>
                </View>
            </View>
        </View>
    );
}



export default AnalyzeCompleted;