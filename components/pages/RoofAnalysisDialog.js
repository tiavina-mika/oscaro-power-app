import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

import Button from '../blocks/Button';
import Link from '../blocks/Link';
import Carousel from '../blocks/Carousel';
import { isTablet, width, height, lg, md } from '../../utils/utils';

const dialogHeight = md? height - 90: height - 100;
let dialogWidth = width - 30;

const styles = StyleSheet.create({
    dialogWidth: {
        width: dialogWidth,
        height: dialogHeight,
        zIndex: 100,
        borderRadius: 10,
        overflow: 'hidden',
        justifyContent: 'space-between'
    },
    card: {
        backgroundColor: '#fff',
        flex: 1,

        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 20
    },
    body: {
        paddingHorizontal: 16,
        alignItems: 'center',
    },
    titleContainer: {
        marginBottom: 5,
        paddingTop: lg? 15: md? 14: 0,
    },
    title: {
        fontSize: lg? 22: md? 20: 18,
        fontWeight: '700'
    },
    instructionContainer: {
        marginTop: lg? 20: md? 16: 0,
        marginBottom: isTablet? 5: 0
    },
    instructionText: {
        textAlign: 'center',
        color: '#757575',
        fontWeight: 'normal',
        fontSize: isTablet? 20: 16,
    },
    action: {
        paddingVertical: lg? 24: md? 25: 12,
    },
    linkContainer: {
        marginTop: lg? 20: md? 22: 10,
        alignItems: 'center'
    },
    buttonContainer: {
        paddingBottom: 20,
        position: 'absolute',
        bottom: 0,
        left: 21
    }
});

const RoofAnalysisDialog = ({ onClose }) => {
    const navigation = useNavigation();

    // React.useLayoutEffect(() => {
    //     navigation.setOptions({ 
    //         headerLeft: () => <Icon name="close"  />,
    //         headerLeftContainerStyle: {marginLeft: 12}
    //     });
    // })
    
    return (
        <View style={styles.dialogWidth}>
            <Carousel onClose={onClose} dialogWidth={dialogWidth} />
            <View style={styles.card}>
                <View style={styles.body}>
                    <View style={styles.titleContainer}>
                        <Text style={styles.title}>Analyser mon toit </Text>
                    </View>
                    <View  style={styles.instructionContainer}>
                        <Text style={styles.instructionText}>Imitez l'inclinaison de votre toit avec votre téléphone et appuyez " valider " pour terminer l'analyze</Text>
                    </View>                    
                </View>

                <View style={styles.action}>
                    <View>
                        <Button
                            title="J'ai compris!"  
                            onPress={() => navigation.navigate('LaunchAnalyze')}
                            width={dialogWidth - 35}
                        />                             
                    </View>
                    <View style={styles.linkContainer}>
                        <Link text="Remplir manuellement" to="Home" />                          
                    </View>
                </View>
            </View>
        </View>
    );
}

export default RoofAnalysisDialog;