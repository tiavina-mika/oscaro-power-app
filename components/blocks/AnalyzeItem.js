import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Icon } from 'react-native-elements';

import { isTablet, md, lg } from '../../utils/utils';

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginVertical: lg? 25: md? 5: 0,
        marginBottom: lg? 25: md? 30: 5,
    },
    main: {
        paddingBottom: 8,
    },
    borderedMain: {
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingBottom: lg? 20: md? 30: 5,
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerBlock: {
        flex: 1,
    },
    headerLeft: {
        alignItems: 'flex-start',
        flexDirection: 'row'
    },
    headerRight: {
        alignItems: 'flex-end'
    },
    headerTitle: {
        fontSize: 18,
        marginLeft: 5,
        fontWeight: '700',
        color: '#1A1A1A'
    },
    chip: {
        backgroundColor: '#EFEFEF',
        paddingHorizontal: 15,
        paddingTop: 5,
        paddingBottom: isTablet? 6: 5,
        borderRadius: 40
    },
    body: {
        alignItems: 'center',
        paddingTop:  isTablet? 6: 0,
        paddingBottom: isTablet? 12: 3,
    },
    descriptionText: {
        fontSize: 16,
        color: '#4D4D4D',
        fontWeight: '300'
    },
    value: {
        fontWeight: '700',
    }
});

const AnalyzeItem = ({ icon, title, value, description, lastItem, iconType }) => {
    return (
        <View style={styles.container}>
            <View style={[styles.main, !lastItem? styles.borderedMain: null]}>
                <View style={styles.header}>
                    <View style={[styles.headerBlock, styles.headerLeft]}>
                        <Icon name={icon} type={iconType} />
                        <Text style={styles.headerTitle}>{ title }</Text>
                    </View>
                    <View style={[styles.headerBlock, styles.headerRight]}>
                        <View style={styles.chip}>
                            <Text style={styles.value}>{value}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.body}>
                    <Text style={styles.descriptionText}>
                        {description}
                    </Text>
                </View>
            </View>
        </View>
    );
}



export default AnalyzeItem;