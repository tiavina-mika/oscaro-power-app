import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Icon } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

import Button from '../blocks/Button';
import Link from '../blocks/Link';
import { lg, md } from '../../utils/utils';

const styles = StyleSheet.create({
    actionContainer: {
        paddingVertical: lg? 20: md? 0: 0,
    },
    linkContainer: {
        marginTop: lg? 20: md? 20: 10,
        alignItems: 'center',
    },
});

const CardAction = ({ width, linkText, linkUrl, buttonText, buttonUrl, icon }) => {
    const navigation = useNavigation();
    return (
        <View style={[styles.actionContainer, { width: width || '100%'}]}>
            <View>
                <Button
                    title={buttonText}  
                    onPress={() => navigation.navigate(buttonUrl)}
                    width="100%"
                    icon={icon}
                />                             
            </View>
            <View style={styles.linkContainer}>
                <Link text={linkText} to={linkUrl} />
            </View>
        </View>
    );
}



export default CardAction;