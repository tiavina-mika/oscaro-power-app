import React from 'react';
import { StyleSheet, View, TouchableHighlight } from 'react-native';
import { Button as CustomButton, Icon  } from 'react-native-elements';
import { width } from '../../utils/utils';

const styles = StyleSheet.create({
    root: {
        backgroundColor: '#000',
        flexDirection: 'row',
        alignItems: 'center',
        height: 56,
        justifyContent: 'space-between',
        borderRadius: 3
    },
    rootFullWidth: {
        width: width - 40,
    },
    rootCustomWidth: width => ({
        width
    }),
    rootButton: {
        flex: 2,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    rootIcon: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        paddingRight: 15,
    },
    containerStyle: {
        marginVertical: 30,
    },
    buttonContainer: {
        height: 56
    },
    button: {
        backgroundColor: 'transparent',
        marginLeft: 15,
        height: 56
    },
    buttonTitle: {
        fontSize: 16,
    },
    buttonTitleBackground: {
        color: '#999'
    }
});

const Button = ({ onPress, title, width, background, icon }) => {
    return (
        <TouchableHighlight onPress={onPress}>
            <View style={[styles.root, !width? styles.rootFullWidth: styles.rootCustomWidth(width || '100%')]}>
                <View style={styles.rootButton}>
                    <CustomButton 
                        title={title} 
                        containerStyle={styles.buttonContainer}
                        buttonStyle={styles.button}
                        titleStyle={[styles.buttonTitle, background? styles.buttonTitleBackground: null]}
                        onPress={onPress}
                    />                
                </View>
                <View style={styles.rootIcon}>
                    <Icon
                        name={icon || "arrow-forward"}
                        color={background? '#999': '#fff'}
                    />
                </View>
            </View>            
        </TouchableHighlight>
    );
}

export default Button;