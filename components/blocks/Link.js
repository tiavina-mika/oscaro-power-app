import React from 'react';
import { StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

const styles = StyleSheet.create({
    link: {
        fontSize: 16,
        color: '#999999',
        textDecorationLine: 'underline',
    },
    bold: {
        fontWeight: '700'
    }
});

const Link = ({ text, to, bold }) => {
    const navigation = useNavigation();

    return (
        <Text
            style={[styles.link, bold? styles.bold: null]}
            onPress={() => navigation.navigate(to)}
        >
            { text }
        </Text>
    );
}

export default Link;