import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { SliderBox } from "react-native-image-slider-box"
import { isTablet, lg, md } from '../../utils/utils';

const imageHeight = lg? 500: md? 350: 300;

const styles = StyleSheet.create({
    closeIconContainer: {
        position: 'absolute',
        zIndex: 105,
        right: 0,
        top: 0,
    },
    imageContainer: {
        height: imageHeight,
    },
    image: {
        height: imageHeight,
    },
});

const Carousel = ({ onClose }) => {
    const [images, setImages] = useState([require('../../assets/images/roof_01.png'), require('../../assets/images/roof_02.png')])
    
    return (
        <View>
            <View style={styles.closeIconContainer}>
                <Icon
                    name="close" 
                    size={isTablet? 42: 30} 
                    color="#fff"
                    onPress={onClose}
                />
            </View>
            <View style={styles.imageContainer}>
                <SliderBox 
                    images={images} 
                    sliderBoxHeight={imageHeight}
                    dotColor="#fff"
                    inactiveDotColor="#B8B8B8"
                />
            </View>
        </View>
    );
}

export default Carousel;