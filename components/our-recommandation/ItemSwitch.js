import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Icon } from 'react-native-elements';
import Switch from './Swtich';


import { isTablet } from '../../utils/utils';

const styles = StyleSheet.create({
    container: padding => ({
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: padding,
        paddingTop: isTablet? 25: 10,
        alignItems: 'center'
    }),
    title: {
        fontSize: isTablet? 18: 16,
        fontWeight: '700',
        marginRight: 10
    },
    header: {
        flexDirection: 'row'
    },
    price: {
        color: '#9B9B9B',
        fontSize: 16
    }
});

const ItemSwitch = ({ paddingHorizontal, title, price, checked, key, onChecked,id }) => {
    return (
        <View style={styles.container(paddingHorizontal)} key={key}>
            <View style={styles.left}>
                <View style={styles.header}>
                    <Text style={styles.title}>{title}</Text>
                    <Icon name="information-outline" type="material-community" color="#7F7F7F" />
                </View>
                <View style={styles.priceContainer}>
                    <Text style={styles.price}>{price}</Text>
                </View>
            </View>
            <View style={styles.right}>
                <Switch checked={checked} onChange={() => onChecked(id)} />
            </View>
        </View>
    );
}

export default ItemSwitch;