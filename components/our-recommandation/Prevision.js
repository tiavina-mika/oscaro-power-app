import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-elements';

import { shadow } from '../../utils/utils';

// const data = [
//     {
//         id: 1,
//         item: 'Economie estimées par an',
//         value: '1200 €'
//     },
//     {
//         id: 2,
//         item: 'Kit rentabilisé en moins de',
//         value: '3 ans'
//     },
//     {
//         id: 3,
//         item: 'Autonomie prévue chaque jour',
//         value: '45 %'
//     },
//     {
//         id: 4,
//         item: 'Economie de CO2 par an',
//         value: '545 Kg'
//     },   
// ];

const styles = StyleSheet.create({
    container: {
        marginVertical: 10,
    },
    block: {
        flexDirection: 'row'
    },
    card: {
        ...shadow(2),
        marginVertical: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        width: '48.5%'
    },
    left: {
        marginRight: 5
    },
    right: {
        marginLeft: 5
    },
    valueContainer: {
        marginTop: 15
    },
    value: {
        fontSize: 22,
        fontWeight: '700'
    }
});

const Prevision = () => {

    return (
        <View style={styles.container}>
            {/* { data.map((item, i) => (
                <View style={styles.card} key={i}>
                    <Text style={styles.item}>{item.item}</Text>
                    <View style={styles.valueContainer}>
                        <Text>{item.value}</Text>
                    </View>                  
                </View>                            
            ))} */}
                <View style={styles.block}>
                    <View style={[styles.card, styles.left]}>
                        <Text style={styles.item}>Economie estimées par an</Text>
                        <View style={styles.valueContainer}>
                            <Text style={styles.value}>1200 €</Text>
                        </View>                  
                    </View>
                    <View style={[styles.card, styles.right]}>
                        <Text style={styles.item}>Kit rentabilisé en moins de</Text>
                        <View style={styles.valueContainer}>
                            <Text style={styles.value}>3 ans</Text>
                        </View>                  
                    </View>
                </View>                     
                <View style={styles.block}>
                    <View style={[styles.card, styles.left]}>
                        <Text style={styles.item}>Autonomie prévue chaque jour</Text>
                        <View style={styles.valueContainer}>
                            <Text style={styles.value}>45 %</Text>
                        </View>                  
                    </View>
                    <View style={[styles.card, styles.right]}>
                        <Text style={styles.item}>Economie de CO2 par an</Text>
                        <View style={styles.valueContainer}>
                            <Text style={styles.value}>545 Kg</Text>
                        </View>                  
                    </View>
                </View>                     
  
        </View>
    );
}

export default Prevision;