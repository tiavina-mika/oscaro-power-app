import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-elements';

const styles = StyleSheet.create({
    mainTitle: {
        textTransform: 'uppercase',
        color: '#999999',
        fontSize: 16
    },
    
});


const BlockTitle = ({ text }) => {

    return (
        <View style={styles.maintTitleContainer}>
            <Text style={styles.mainTitle}>{ text }</Text>
        </View>
    );
}

export default BlockTitle;