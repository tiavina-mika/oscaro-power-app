import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, Slider as Slide } from 'react-native-elements';

const rayon = 40;
const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    left: {
        paddingRight: 10
    },
    right: {
        paddingLeft: 10
    },   
    slider: {
        flex: 1, 
        alignItems: 'stretch', 
        justifyContent: 'center'
    },
    trackStyle: {
        height: 10, 
        borderRadius: 5
    },
    thumbStyle: {
        backgroundColor: '#000', 
        width: rayon, 
        height: rayon, 
        borderRadius: rayon / 2,
        borderColor: '#fff',
        borderWidth: 5
    }
});

const Slider = () => {

    return (
        <View style={styles.container}>
            <View style={[styles.value, styles.left]}>
                <Text>4</Text>
            </View>
            <View style={styles.slider}>
                <Slide
                    value={0.8}
                    trackStyle={styles.trackStyle}
                    thumbStyle={styles.thumbStyle}
                    // onValueChange={value => this.setState({ value })}
                />                
            </View>

            <View style={[styles.value, styles.right]}>
                <Text>20</Text>
            </View>
            {/* <Text>Value: {this.state.value}</Text> */}
        </View>
    );
}

export default Slider;