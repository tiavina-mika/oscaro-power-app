import React from 'react';
import Switches from 'react-native-switches'

import { isTablet } from '../../utils/utils';

const Switch = ({ checked, onChange }) => {
    return (
        <Switches
            shape={'pill'} 
            value={checked || false} 
            showText={false} 
            buttonOffsetLeft={isTablet? 5: 3}
            buttonOffsetRight={isTablet? 5: 6}
            buttonSize={isTablet? 28: 19}
            sliderWidth={isTablet? 60: 42}
            sliderHeight={isTablet? 40: 30}
            colorSwitchOff="#D8D8D8"
            colorSwitchOn="#000"
            onChange={onChange}
        />
    );
}

export default Switch;