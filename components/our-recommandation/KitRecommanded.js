import React, { useEffect, useState } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import { Text, Icon } from 'react-native-elements';
import ItemSwitch from './ItemSwitch';
import { isTablet, shadow } from '../../utils/utils';

const cardPadding = 20;
const styles = StyleSheet.create({
    card: {
        ...shadow(3),
        paddingVertical: 20,
        marginTop: 12
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingBottom: 10
    },
    cardPadding: {
        paddingHorizontal: cardPadding
    },
    divider: {
        height: 1,
        backgroundColor: '#D8D8D8'
    },
    headerTitle: {
        fontSize: isTablet? 20: 18,
        fontWeight: '700'
    },
    imageContainer: {
        flex: 1,
        alignItems: 'flex-start',
    },
    headerTitleContainer: {
        flex: isTablet? 4: 3,
        alignItems: 'center',
    },
    priceContainer: {
        flex: 2,
        alignItems: 'flex-end'
    },
    price: {
        paddingHorizontal: isTablet? 15: 8,
        backgroundColor: '#EFEFEF',
        paddingVertical: 5,
        borderRadius: 5,
        fontWeight: '600'
    }
});

const items = [
    {
        id: 1,
        item: 'Régulateur chauffe-eau',
        price: '15 €',
    },
    {
        id: 2,
        item: "Stockage d'énergie",
        price: '3400 €',
    }, 
];

const KitRecommanded = ({ navigation }) => {
    const [data, setData] = useState([]);

    useEffect(() => {
        const newData = [...data, ...items.map(item => ({checked: false, ...item}))];     
        setData(newData);
    }, [items])

    const toggle = index => {
        const newData = [...data];
        const currentDataIndex = newData.findIndex(d => d.id === index);
        newData[currentDataIndex].checked = !newData[currentDataIndex].checked;

        setData([...newData]);
    }

    return (
        <>
            <View style={[styles.card]}>
                <View style={[styles.header, styles.cardPadding]}>
                    <View style={styles.imageContainer}>
                        <Icon name="contact-phone" />
                    </View>
                    <View style={styles.headerTitleContainer}>
                        <Text style={styles.headerTitle}>Installation pour 8 panneaux solaires</Text>
                    </View>
                    <View style={styles.priceContainer}>
                        <Text style={styles.price}>3400 €</Text>
                    </View>
                </View>
                <View style={styles.divider}></View>
                <View style={styles.body}>
                    { data
                    ? data.map((d, i) => (
                            i <= 1 &&
                            <ItemSwitch
                            key={i}
                            paddingHorizontal={cardPadding} 
                            title={d.item}
                            id={d.id}
                            price={d.price}
                            checked={d.checked}
                            onChecked={toggle}
                        />
                    ))
                    :  <ActivityIndicator size="large" color="#0000ff" />
                }
                </View>
            </View>
        </>
    );
}

export default KitRecommanded;