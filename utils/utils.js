import { Dimensions } from 'react-native';

export const width = Dimensions.get("window").width;
export const height = Dimensions.get("window").height;

export const isTablet = width > 420;

export const getScreen = () =>{
        let screen = 'md'
        if (width <= 360) {
            screen = 'sm';
        } else if ( width > 360 && width <= 420){
            screen = 'md';
        // } else (width > 420) {
        } else {
            screen = 'lg';
        }
        return screen;
};

export const sm = getScreen() === 'sm';
export const md = getScreen() === 'md';
export const lg = getScreen() === 'lg';

export const shadow = elevation => { 
    return {
        backgroundColor: '#fff',
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: elevation },
        shadowOpacity: 1,
        shadowRadius: 2 * elevation,
    };
};