import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { ThemeProvider } from 'react-native-elements';

import Home from './components/pages/Home';
import YourSituation from './components/pages/YourSituation';
import LaunchAnalyze from './components/pages/LaunchAnalyze';
import AnalyzeCompleted from './components/pages/AnalyzeCompleted';
import OurRecommandation from './components/pages/OurRecommandation';

const Stack = createStackNavigator();

const theme = {
    Text: {
        style: {
            fontSize: 16
        },
    },
};

const App = () => {
    return (
        <ThemeProvider theme={theme}>
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Home">
                    <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
                    <Stack.Screen 
                        name="YourSituation"  
                        options={{ 
                            title: '',  
                            headerStyle: { elevation: 0, shadowOpacity: 0 } 
                        }}
                        component={YourSituation} 
                    />
                    <Stack.Screen 
                        name="LaunchAnalyze"
                        component={LaunchAnalyze}  
                        options={{ headerShown: false }} />
                    <Stack.Screen 
                        name="AnalyzeCompleted"
                        component={AnalyzeCompleted}  
                        options={{ headerShown: false }} />
                    <Stack.Screen 
                        name="OurRecommandation"  
                        options={{ 
                            title: 'Notre Recommandation',  
                            headerTitleAlign: 'center',
                            headerTitleStyle: { fontWeight: '700' },
                            headerStyle: { elevation: 0, shadowOpacity: 0 } 
                        }}
                        component={OurRecommandation} 
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </ThemeProvider>
    );
}

export default App;